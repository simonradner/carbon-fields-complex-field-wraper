<?php

namespace Sef\CarbonFieldsComplexFieldWraper;
use \Carbon_Fields\Field\Field as CarbonFieldField;
use Carbon_Fields\Field\Complex_Field as CarbonFieldsComplexField;

class Field
{
  public $name;

  public $baseName;

  public $value;

  protected $field;

  public setField( CarbonFieldField $field )
  {
    $this->field = $field
  }

  public isComplex()
  {
    return ( $this->field instanceof CarbonFieldsComplexField );
  }
}
