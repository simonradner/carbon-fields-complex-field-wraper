<?php
namespace Sef\CarbonFieldsComplexFieldWraper\Converter;

use Sef\CarbonFieldsComplexFieldWraper\Field;

use Sef\WpEntities\Components\Converter\Converter;
use Sef\WpEntities\Interfaces\ConverterInterface;
use Carbon_Fields\Field\Complex_Field as CarbonFieldsComplexField;
use Carbon_Fields\Field\Field as CarbonFieldsField;
use Doctrine\Common\Collections\ArrayCollection;


class ComplexField2Wraper extends Converter implements ConverterInterface {

  protected function converting($data){

    if( ! $data instanceof CarbonFieldsField )
      return null;

    $converted = new Field;

    switch($data)
    {
      default:
      $converted->baseName = $data->get_base_name();
      $converted->name = $data->get_name();
      $converted->value = $data->get_value();
      $converted->setField($data);
      break;
    }

    return $converted;

  }

  protected function convertingCollection( $data )
  {

    if( $data instanceof Complex_Field )
    {
      $converted = new Field;
      $converted->baseName = $data->get_base_name();
      $converted->name = $data->get_name();
      $converted->setField($data);

      $collection = new ArrayCollection;
      $iterateableData = $data->get_values();
      foreach( $iterateableData as $i => $itemdata_what )
      {
        $collectionItem = [];
        foreach( $itemdata_what as $itemdata )
        {
          // echo'<pre>';  print_r(var_dump($itemdata)); echo'</pre>';
          $converter = new static();
          $converted  = $converter->setData($itemdata)->convert();
          $collectionItem[] = $converted ;
        }
        $collection[] = $collectionItem ;

      }

      $converted->value = $collection;

      return $converted;

    }
  }


  protected function isConverted( $data )
  {
    return false;
  }


  protected function isCollection( $data )
  {
    return ($data instanceof CarbonFieldsComplexField );
  }

  protected function setup()
  {
    //
  }

}
