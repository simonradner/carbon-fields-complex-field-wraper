<?php
namespace Sef\CarbonFieldsComplexFieldWraper;
use Carbon_Fields\Field\Complex_Field as CarbonFieldsComplexField;

class CarbonFieldsComplexFieldWraper
{
  public $field;

  protected $_field;

  public function __construct( CarbonFieldsComplexField $complexfield )
  {
    $converter = new Converter\ComplexField2Wraper;
    $this->_field = $complexfield;
    $this->field = $converter->setData($complexfield)->convert();
  }
}
